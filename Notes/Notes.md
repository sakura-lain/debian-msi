# Installation et configuration de Debian sur un MSI U130

Réglages pour le MSI U130 sous Debian 9 (mise à jour en Debian 10) avec environnement Xfce. 
- L'ordinateur est équipé d'un clavier Qwerty modifié en Azerty. 
- Il est en multiboot Windows XP Pro, Debian, CentOS et FreeBSD.

**[Voir le Wiki >>>](https://gitlab.com/sakura-lain/debian-msi/wikis/home)**

Voir aussi, pour le même ordinateur :

- [CentOS](https://gitlab.com/sakura-lain/centos-msi)
- [FreeBSD](https://gitlab.com/sakura-lain/freebsd-msi)

## Paramètres clavier :

Clavier générique 104 touches (défaut : 105 intl)
Fr variante, latin 9 uniquement (défaut : variante obsolète)

## Ajout des signes < et > sur la touche Windows :

- installation du paquet `xorg-xmodmap`
- création dans `/home/username` d’un fichier `.Xmodmap` contenant la ligne :
	
```
keycode 133 = 60 62
```
    
(utilisation de `xev` pour connaître le keycode)

- création d’un fichier `Xmodmap` contenant les lignes :

```
# /bin/sh
xmodmap ~/.Xmodmap
```
    
- dans Applications>Paramètres>Session et démarrage>Démarrage automatique, ajout de la commande :
   
```
bash /home/moi/Xmodmap.sh
```
	
Pour lui attribuer la touche Compose, changer `.Xmodmap` en :
    
```
bash /home/moi/Xmodmap.sh
```
	
Refaire l’opération pour chaque utilisateur.

Alternative : mettre le fichier `Xmodmap` dans `/home` (voir [installation FreeBSD](https://gitlab.com/sakura-lain/freebsd-msi))

[Forum Ubuntu-fr](https://forum.ubuntu-fr.org/viewtopic.php?id=306533)

## Pour activer le tapotement sur le touchpad :

- l’installation du paquet `xserver-xorg-input-synaptics` ajoute un onglet « pavé tactile » aux paramètre de la souris.
Ou :
- en ligne de commandes (attention manip à refaire à chaque allumage) :

```
$ synclient TapButton1=1
$ synclient TapButton2=2
$ synclient TapButton3=3
```
	
- ou :

```
$ synclient TapButton1=1 TapButton2=2 TapButton3=3
```
    
Par défaut, toutes ces valeurs sont à 0

- vérifier le réglage en tapant :

```
$ synclient
```

[Ubuntu-fr](https://doc.ubuntu-fr.org/touchpad)
[Debian facile](https://debian-facile.org/viewtopic.php?id=6734)

## Changer un fond d’écran : 

clic droit sur l’image à afficher, « définir comme fond d’écran »

## Wine :

- installer :

```
dpkg --add-architecture i386 && apt-get update && apt-get install wine32
```

- désinstaller  : supprimer si nécessaire les fichiers dans `$HOME/` et dans `$HOME/.local/share/applications/`

## Tuto Multisystem : 

[Ubuntu-fr](https://doc.ubuntu-fr.org/multisystem)

pour `usermod`, appliquer la 3e méthode (avec le nom d’utilisateur) : 
	
```
usermod -a -G adm "sakura"
```

## Alias :

- pour créer ou débloquer définitivement des alias : ajouter et décommenter dans `~/.bashrc` de `/home/user`
- Répéter l’opération pour chaque utilisateur. Pour root, le fichier se trouve dans `/root`

## sudo :

Si besoin de sudo, penser à activer le service.

## Grub :

- Vérifier que Grub est présent : 

```
grub-install -V
```

- Réinstaller Grub : 

```
grub-install /dev/sda
```

- Détecter les OS :

```
os-prober
```

- Mettre à jour Grub : 

```
update-grub
```

### Ajouter FreeBSD à Grub2 :

- éditer `/etc/grub.d/40_custom`
- y inscrire le texte :

```
menuentry "FreeBSD 11.1 (sur /dev/sda4)" {
set root=(hd0,msdos4) 
chainloader +1
}
```
(msdos = table MBR, 4 = numéro de la partition primaire contenant FreeBSD, soit `/dev/sda4`)

- entrer les commandes `grub-mkconfig` et `update-grub`

### Changer l’ordre de boot dans Grub2 :

(ici : passer de Debian>XP>CentOS>FreeBSD à Debian>CentOS>FreeBSD>XP)

- si ce n’est déjà fait, éxécuter `os-prober`
- éditer `/boot/grub/grub.cfg`
- copier les différentes entrées et les remettre dans l’ordre dans `/etc/grub.d/40_custom`, ce qui nous donne :

```
#!/bin/sh
exec tail -n +3 $0
# This file provides an easy way to add custom menu entries.  Simply type the
# menu entries you want to add after this comment.  Be careful not to change
# the 'exec tail' line above.

menuentry 'CentOS Linux 7 (Core) (sur /dev/sda7)' --class centos --class gnu-linux --class gnu --class os $menuentry_id_option 'osprober-gnulinux-simple-2edb843e-9e69-43f1-b1f3-4429f918ccb1' {
	insmod part_msdos
	insmod ext2
	set root='hd0,msdos7'
	if [ x$feature_platform_search_hint = xy ]; then
	  search --no-floppy --fs-uuid --set=root --hint-bios=hd0,msdos7 --hint-efi=hd0,msdos7 --hint-baremetal=ahci0,msdos7  2edb843e-9e69-43f1-b1f3-4429f918ccb1
	else
	  search --no-floppy --fs-uuid --set=root 2edb843e-9e69-43f1-b1f3-4429f918ccb1
	fi
	linux /boot/vmlinuz-0-rescue-113f25f0e6b046a89ab7630fa791d894 root=/dev/sda7
	initrd /boot/initramfs-0-rescue-113f25f0e6b046a89ab7630fa791d894.img
}
submenu 'Options avancées pour CentOS Linux 7 (Core) (sur /dev/sda7)' $menuentry_id_option 'osprober-gnulinux-advanced-2edb843e-9e69-43f1-b1f3-4429f918ccb1' {
	menuentry 'CentOS Linux 7 (Core) (sur /dev/sda7)' --class gnu-linux --class gnu --class os $menuentry_id_option 'osprober-gnulinux-/boot/vmlinuz-0-rescue-113f25f0e6b046a89ab7630fa791d894--2edb843e-9e69-43f1-b1f3-4429f918ccb1' {
		insmod part_msdos
		insmod ext2
		set root='hd0,msdos7'
		if [ x$feature_platform_search_hint = xy ]; then
		  search --no-floppy --fs-uuid --set=root --hint-bios=hd0,msdos7 --hint-efi=hd0,msdos7 --hint-baremetal=ahci0,msdos7  2edb843e-9e69-43f1-b1f3-4429f918ccb1
		else
		  search --no-floppy --fs-uuid --set=root 2edb843e-9e69-43f1-b1f3-4429f918ccb1
		fi
		linux /boot/vmlinuz-0-rescue-113f25f0e6b046a89ab7630fa791d894 root=/dev/sda7
		initrd /boot/initramfs-0-rescue-113f25f0e6b046a89ab7630fa791d894.img
	}
	menuentry 'CentOS Linux 7 (Core) (sur /dev/sda7)' --class gnu-linux --class gnu --class os $menuentry_id_option 'osprober-gnulinux-/boot/vmlinuz-3.10.0-693.el7.x86_64--2edb843e-9e69-43f1-b1f3-4429f918ccb1' {
		insmod part_msdos
		insmod ext2
		set root='hd0,msdos7'
		if [ x$feature_platform_search_hint = xy ]; then
		  search --no-floppy --fs-uuid --set=root --hint-bios=hd0,msdos7 --hint-efi=hd0,msdos7 --hint-baremetal=ahci0,msdos7  2edb843e-9e69-43f1-b1f3-4429f918ccb1
		else
		  search --no-floppy --fs-uuid --set=root 2edb843e-9e69-43f1-b1f3-4429f918ccb1
		fi
		linux /boot/vmlinuz-3.10.0-693.el7.x86_64 root=/dev/sda7
		initrd /boot/initramfs-3.10.0-693.el7.x86_64.img
	}
}

menuentry "FreeBSD 11.1 (sur /dev/sda4)" {
set root=(hd0,msdos4)
chainloader +1
}

menuentry 'Microsoft Windows XP Professionnel (sur /dev/sda1)' --class windows --class os $menuentry_id_option 'osprober-chain-FE0CF38C0CF33E69' {
	insmod part_msdos
	insmod ntfs
	set root='hd0,msdos1'
	if [ x$feature_platform_search_hint = xy ]; then
	  search --no-floppy --fs-uuid --set=root --hint-bios=hd0,msdos1 --hint-efi=hd0,msdos1 --hint-baremetal=ahci0,msdos1  FE0CF38C0CF33E69
	else
	  search --no-floppy --fs-uuid --set=root FE0CF38C0CF33E69
	fi
	parttool ${root} hidden-
	drivemap -s (hd0) ${root}
	chainloader +1
}
```

- dans `/etc/default/grub`, ajouter une ligne en fin de fichier pour désactiver `os-prober` (il faudra le réactiver en cas, par exemple de réinstallation de Windows XP – pour FreeBSD ce n’est semble-t-il pas la peine) :
	
```
GRUB_DISABLE_OS_PROBER=true
```

- Faire un `update-grub`

[Forums Ubuntu-fr](https://forum.ubuntu-fr.org/viewtopic.php?id=1988801)

### Passer le temps d’affichage du Grub de 5 à 10 secondes :

- dans `/etc/default/grub`,  faire passer la valeur de la ligne `GRUB_TIMEOUT` de 5 à 10.
- Faire un `update-grub`

## Désactiver le lancement automatique de Firefox à l’ouverture d’une session :

- vérifier que l’enregistrement de session est décoché à la déconnexion
- dans « paramètres > session et démarrage », vérifier que Firefox n’apparaît pas dans les menus « démarrage automatique » et « session ». Si c’est le cas, décocher/supprimer
- éventuellement : « effacer les sessions enregistrées »

[Debian-fr](https://www.debian-fr.org/t/demarrage-automatique-de-firefox/72687) 

## Antivirus :

L’interface graphique pour Clamav sous Xfce est Clamtk (sous KDE : Klamav)

## Installer un paquet hors dépôt :

- se placer dans le répertoire contenant le paquet concerné
`gdebi install nom_du_paquet` ou `dpkg install nom_du_paquet`

[Ubuntu-fr](https://doc.ubuntu-fr.org/gdebi)

## locate :

- installer `locate`
- `locate nom_du_fichier`
- penser à mettre à jour la BDD : `updatedb`

## OpenBox :

- [Tux Planet](http://www.tux-planet.fr/openbox-installation-configuration-et-utilisation/)
- [Ata Boy Design](http://blog.ataboydesign.com/2013/12/28/freebsd-10-rc4-installation-and-configuration-for-openbox/)

## Autres

- `ncdu` : utilitaire d’affichage d’espace disque (en graphique : Baobab)
- `fslint` : recherche de doublons
- Nécessité de passer par Windows XP pour rendre reconnaissable une clé USB Wifi sous Linux.