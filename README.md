# Installation et configuration de Debian sur un MSI U130

Réglages pour le MSI U130 sous Debian 9 (mise à jour en Debian 10) avec environnement Xfce. 
- L'ordinateur est équipé d'un clavier Qwerty modifié en Azerty. 
- Il est en multiboot Windows XP Pro, Debian, CentOS et FreeBSD.

**[Voir le Wiki >>>](https://gitlab.com/sakura-lain/debian-msi/wikis/home)**

Voir aussi, pour le même ordinateur :

- [CentOS](https://gitlab.com/sakura-lain/centos-msi)
- [FreeBSD](https://gitlab.com/sakura-lain/freebsd-msi)